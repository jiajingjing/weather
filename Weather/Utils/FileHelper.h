//
//  FileHelper.h
//  Weather
//
//  Created by jingjing jia on 2023/4/27.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface FileHelper : NSObject

+ (id)loadBundledJSON:(NSString *)file;

+ (id)loadJSONFromURL:(NSURL *)url;

+ (id)loadJSONFromDirectory:(NSSearchPathDirectory)directory fileName:(NSString *)fileName;

+ (void)writeJSON:(id)value toURL:(NSURL *)url;

+ (void)writeJSON:(id)value toDirectory:(NSSearchPathDirectory)directory fileName:(NSString *)fileName;

+ (void)deleteFileIn:(NSSearchPathDirectory)directory fileName:(NSString *)fileName;

@end

NS_ASSUME_NONNULL_END
