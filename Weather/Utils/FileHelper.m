//
//  FileHelper.m
//  Weather
//
//  Created by jingjing jia on 2023/4/27.
//

#import "FileHelper.h"

@implementation FileHelper

+ (id)loadBundledJSON:(NSString *)file {
    NSURL *url = [[NSBundle mainBundle] URLForResource:file withExtension:@"json"];
    if (!url) {
        return nil;
    }

    return [self loadJSONFromURL:url];
}

+ (id)loadJSONFromURL:(NSURL *)url {
    NSError *error = nil;
    NSData *data = [NSData dataWithContentsOfURL:url options:NSDataReadingMappedIfSafe error:&error];
    if (!data) {
        return nil;
    }
    if (error != nil) {
        NSLog(@"Error load URL Content : %@", error);
    }
    return data;
    
//    id value = [NSJSONSerialization JSONObjectWithData:data options:0 error:&error];
//    if (!value) {
//        return nil;
//    }
//    if (error != nil) {
//        NSLog(@"Error JSON : %@", error);
//    }
//
//    return value;
}

+ (id)loadJSONFromDirectory:(NSSearchPathDirectory)directory fileName:(NSString *)fileName {
    NSURL *url = [[NSFileManager defaultManager] URLsForDirectory:directory inDomains:NSUserDomainMask].firstObject;
    if (!url) {
        return nil;
    }
    
    url = [url URLByAppendingPathComponent:fileName];
    return [self loadJSONFromURL:url];
}

+ (void)writeJSON:(id)value toURL:(NSURL *)url{
    
    NSError *error = nil;
    NSData *data = [NSJSONSerialization dataWithJSONObject:value options:0 error:&error];
    if (!data) {
        return;
    }
    if (error != nil) {
        NSLog(@"Error JSON : %@", error);
    }

    [data writeToURL:url options:NSDataWritingAtomic error:&error];
    if (error != nil) {
        NSLog(@"Error JSON : %@", error);
    }
}

+ (void)writeJSON:(NSString *)jsonString to:(NSSearchPathDirectory)directory fileName:(NSString *)fileName {
    NSString *filePath = [self filePathForDirectory:directory fileName:fileName];
    
    NSError *error = nil;
    [jsonString writeToFile:filePath atomically:YES encoding:NSUTF8StringEncoding error:&error];
    
    if (error != nil) {
        NSLog(@"Error writing JSON to file: %@", error);
    }
}

+ (void)writeJSON:(id)value toDirectory:(NSSearchPathDirectory)directory fileName:(NSString *)fileName{
    NSURL *url = [[NSFileManager defaultManager] URLsForDirectory:directory inDomains:NSUserDomainMask].firstObject;
    if (!url) {
        return;
    }
    
    url = [url URLByAppendingPathComponent:fileName];
    [self writeJSON:value toURL:url];
}

+ (void)deleteFileIn:(NSSearchPathDirectory)directory fileName:(NSString *)fileName {
    NSString *filePath = [self filePathForDirectory:directory fileName:fileName];
    
    NSError *error = nil;
    [[NSFileManager defaultManager] removeItemAtPath:filePath error:&error];
    
    if (error != nil) {
        NSLog(@"Error deleting file: %@", error);
    }
}

+ (NSString *)filePathForDirectory:(NSSearchPathDirectory)directory fileName:(NSString *)fileName {
    NSArray *paths = NSSearchPathForDirectoriesInDomains(directory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString *filePath = [documentsDirectory stringByAppendingPathComponent:fileName];
    return filePath;
}

@end
