//
//  WeatherHeaderViewModel.swift
//  Weather
//
//  Created by jingjing jia on 2023/4/28.
//

import Foundation
import SwiftUI

struct WeatherHeaderViewModel {
    let live: Live
    
    //MARK: - Date Formatter

    private static var dateFormatter: DateFormatter{
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "E, MM, d"
        return dateFormatter
    }
    
    //MARK: - Number Formatter
    private static var numberFormatter: NumberFormatter {
        let numberFormatter = NumberFormatter()
        numberFormatter.maximumFractionDigits = 1
        return numberFormatter
    }
    
    private static var numberFormatter2: NumberFormatter {
        let numberFormatter = NumberFormatter()
        numberFormatter.numberStyle = .percent
        return numberFormatter
    }
    
    
    //MARK: - Computed Properties

    var province: String {
        return live.province
    }
    
    var city: String {
        return live.city
    }
    
    var adcode : String {
        return live.adcode
    }
    
    var weather : String {
        return live.weather
    }
    
    var temperature : String {
        return "\(live.temperature)°"
    }
    var wind : String {
        return "\(live.winddirection)东风 \(live.windpower)级"
    }
    
    var winddirection : String {
        return live.winddirection
    }
    
    var windpower : String {
        return live.windpower
    }
    
    var humidity : String {
        return live.humidity
    }
    
    var reporttime : String {
        return live.reporttime
    }
}
