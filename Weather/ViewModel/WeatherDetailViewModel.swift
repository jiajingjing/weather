//
//  WeatherDetailViewModel.swift
//  Weather
//
//  Created by jingjing jia on 2023/4/26.
//

import Foundation
import SwiftUI

struct WeatherDetailViewModel {
    let forecast: Cast
    
    let weekdays = ["星期一", "星期二", "星期三", "星期四", "星期五", "星期六","星期日"]

    
    //MARK: - Date Formatter

    private static var dateFormatter: DateFormatter{
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "E, MM, d"
        return dateFormatter
    }
    
    //MARK: - Number Formatter
    private static var numberFormatter: NumberFormatter {
        let numberFormatter = NumberFormatter()
        numberFormatter.maximumFractionDigits = 1
        return numberFormatter
    }
    
    private static var numberFormatter2: NumberFormatter {
        let numberFormatter = NumberFormatter()
        numberFormatter.numberStyle = .percent
        return numberFormatter
    }
    
    func getWeatherImageName(for condition: String) -> String {
        switch condition {
        case "晴":
            return "sun.max.fill"
        case "多云", "少云":
            return "cloud.sun.fill"
        case "阴":
            return "cloud.fill"
        case "雾":
            return "cloud.fog.fill"
        case "小雨", "中雨", "大雨", "阵雨":
            return "cloud.rain.fill"
        case "小雪", "中雪", "大雪", "暴雪":
            return "cloud.snow.fill"
        case "雷阵雨":
            return "cloud.bolt.rain.fill"
        default:
            return "questionmark.diamond.fill"
        }
    }

    
    
    //MARK: - Computed Properties

    var day: String {
        return forecast.date
    }
    
    var week: String {
        return weekdays[(Int(forecast.week) ?? 1) - 1]
    }
    
    var dayWeather : String {
        return getWeatherImageName(for: forecast.dayweather)
    }
    
    var nightWeather : String {
        return forecast.nightweather
    }
    
    var dayTemp : String {
        return "H: \(forecast.daytemp)°"
    }
    
    var nightTemp : String {
        return "L: \(forecast.nighttemp)°"
    }
    
    var dayTempValue : String {
        return forecast.daytemp_float
    }
    
    var nightTempValue : String {
        return forecast.nighttemp_float
    }

    
    enum ActiveSheet: Identifiable {
        case searchHistory, setting
        
        var id: Int {
            hashValue
        }
    }
}
