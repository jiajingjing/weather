//
//  ContentView.swift
//  Weather
//
//  Created by jingjing jia on 2023/4/25.
//

import SwiftUI

struct ContentView: View {
    
    @EnvironmentObject var weatherListVM: WeatherListViewModel
    @State private var showSheet = false

    var body: some View {
        ZStack {
            NavigationView {
                VStack {
                    WeatherHeaderView(weatherListViewModel: _weatherListVM)
                    WeatherListView(weatherListViewModel: _weatherListVM)
                }
                .padding(.horizontal)
                .navigationTitle("Weather")
                .toolbar {
                    ToolbarItemGroup(placement: .navigationBarLeading) {
                        Button {
                            showSheet = true
                        } label: {
                            Text(self.weatherListVM.choosedCity?.name ?? "")
                        }
                    }
                }
            }
        }.sheet(isPresented: $showSheet) {
            CityListView()
                .environmentObject(self.weatherListVM)
        }
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
