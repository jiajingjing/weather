//
//  CityListView.swift
//  Weather
//
//  Created by jingjing jia on 2023/4/27.
//

import SwiftUI

struct CityListView: View {

    @Environment(\.presentationMode) var presentationMode
    @EnvironmentObject var weatherListVM: WeatherListViewModel

    var body: some View {
        NavigationView {
            List(weatherListVM.cities,id:\.self, selection:$weatherListVM.choosedCity) { city in
//            List(weatherListVM.cities) { city in
                Text("\(city.name )")
                    .padding(.trailing, 16)
                    .padding(.leading, 16)
                    .onTapGesture {
                        print(city.name )
                        weatherListVM.choosedCity = city
                        weatherListVM.fetchWeatherLive()
                        weatherListVM.fetchWeatherForecase()
                        self.presentationMode.wrappedValue.dismiss()
                    }
            }

            .navigationTitle("City List")
            .toolbar {
                ToolbarItemGroup(placement: .navigationBarTrailing) {
                    Button(action: {
                        self.presentationMode.wrappedValue.dismiss()
                    }, label: {
                        Text("Done")
                    })
                }
            }
        }
    }
}

struct CityListView_Previews: PreviewProvider {
    static var previews: some View {
        CityListView()
    }
}
