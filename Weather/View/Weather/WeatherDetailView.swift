//
//  WeatherDataView.swift
//  Weather
//
//  Created by jingjing jia on 2023/4/27.
//

import SwiftUI

struct WeatherDetailView: View {
    
    var day = WeatherListViewModel().forecasts[0]
        
    var body: some View {
        VStack(alignment: .leading) {
            
            HStack(alignment: .center) {
                HStack{
                    Text(day.week)
                        .font(.system(size: 14, weight: .medium))
                    Spacer()
                    Image(systemName: day.dayWeather)
                        .renderingMode(.original)
                }.padding(.trailing, 50)
                
                HStack(alignment: .center, spacing: nil) {
                    Text(day.dayTemp)
                        .font(.system(size: 14, weight: .medium))
                    ProgressView(value: 0.1)
                        .padding(.top, 10)
                        .padding(.bottom, 10)
                        .padding(.top, 8)
                    Text(day.nightTemp)
                        .font(.system(size: 14, weight: .medium))
                }
            }
        }
    }
}

struct WeatherDetailView_Previews: PreviewProvider {
    static var previews: some View {
        WeatherDetailView()
    }
}
