//
//  WeatherHeaderView.swift
//  Weather
//
//  Created by jingjing jia on 2023/4/27.
//

import SwiftUI

struct WeatherHeaderView: View {
    
    @EnvironmentObject var weatherListViewModel: WeatherListViewModel

    var body: some View {
        VStack(alignment: .center, spacing: 0){
            Text(weatherListViewModel.lives.first?.city ?? "--")
                .font(.system(size: 30, weight: .regular, design: .default))
            
            Text(weatherListViewModel.lives.first?.temperature ?? "--")
                .font(.system(size: 100, weight: .thin, design: .default))
                .offset(CGSize(width: 10, height: 0))
                .padding(.top, -10)
                .padding(.bottom, -10)
            
            Text(weatherListViewModel.lives.first?.weather ?? "--")
                .font(.system(size: 18, weight: .medium, design: .default))
            
            Text(weatherListViewModel.lives.first?.wind ?? "--")
                .font(.system(size: 18, weight: .medium, design: .default))
        }
        .padding(.bottom, 68)
        .padding(.top, 74)
    }
}

struct WeatherHeaderView_Previews: PreviewProvider {
    static var previews: some View {
        WeatherHeaderView()
    }
}
