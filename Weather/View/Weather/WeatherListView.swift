//
//  WeatherListView.swift
//  Weather
//
//  Created by jingjing jia on 2023/4/27.
//

import SwiftUI

struct WeatherListView: View {
    
    @EnvironmentObject var weatherListViewModel: WeatherListViewModel
    
    var body: some View {
        VStack(alignment: .leading) {
            HStack{
                Image(systemName: "calendar")
                Text("天气预报")
                    .font(.system(size: 13, weight: .regular))
                Spacer()
            }
            .padding(.top, 10)
            .padding(.leading, 15)
            List(weatherListViewModel.forecasts, id: \.day) { day in
                WeatherDetailView(day: day)
            }
        }
    }
}

struct WeatherListView_Previews: PreviewProvider {
    static var previews: some View {
        WeatherListView()
    }
}
