//
//  CityModel.swift
//  Weather
//
//  Created by jingjing jia on 2023/4/26.
//

import Foundation

class CityModel: Codable, Hashable {
    var id: Int
    var name: String
    var adcode: Int
    
    init(id: Int, name: String, adcode: Int) {
        self.id = id
        self.name = name
        self.adcode = adcode
    }
    
    static func == (lhs: CityModel, rhs: CityModel) -> Bool {
        return lhs.id == rhs.id
    }
    
    func hash(into hasher: inout Hasher) {
        hasher.combine(id)
    }
}
