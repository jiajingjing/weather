//
//  WeatherModel.swift
//  Weather
//
//  Created by jingjing jia on 2023/4/26.
//

import Foundation


struct Forecast: Codable {
    let city: String
    let adcode: String
    let reporttime: String
    let casts: [Cast]
}

struct Cast: Codable {
    let date: String
    let week: String
    let dayweather: String
    let nightweather: String
    let daytemp: String
    let nighttemp: String
    let daytemp_float: String
    let nighttemp_float: String
    let daywind: String
    let nightwind: String
    let daypower: String
    let nightpower: String
}

struct Live: Codable {
    let province: String
    let city: String
    let adcode: String
    let weather: String
    let temperature: String
    let winddirection: String
    let windpower: String
    let humidity: String
    let reporttime: String
}

struct WeatherLiveModel: Codable {
    
    let status: String
    let count: String
    let info: String
    let infocode: String
    
    let lives: [Live]
}

extension WeatherLiveModel {
    
}


struct WeatherForecastModel: Codable {
    
    let status: String
    let count: String
    let info: String
    let infocode: String
    
    let forecasts: [Forecast]
}


